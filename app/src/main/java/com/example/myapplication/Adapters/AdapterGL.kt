package com.example.myapplication.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListAdapter
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.Interfaces.IIntroInterface
import com.example.myapplication.R
import com.example.myapplication.models.LoadingState
import com.example.myapplication.models.Recipe
import kotlinx.android.synthetic.main.example_list.view.*
import java.lang.Exception



class GroceryListAdapter(private val controller:IIntroInterface):RecyclerView.Adapter<GLViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GLViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.example_list,parent, false)
        val vh = GLViewHolder(view)

        view.remove_btn.setOnClickListener {
            val position = vh.adapterPosition
            controller.runAsync {
                try {
                    view.alpha = 0.5f
                    controller.deleteItem(position)
                    notifyItemRemoved(position)
                } catch (e: Exception) {
                    Toast.makeText(parent.context, "Failed to delete item", Toast.LENGTH_SHORT).show()
                    view.alpha = 1.0f
                }
            }
            //controller.deleteItem(position)
            //notifyItemRemoved(position)
        }
        view.setOnLongClickListener {
            controller.runAsync {
                try {
                    view.alpha = 0.5f
                    controller.deleteItem(vh.adapterPosition)
                    notifyItemRemoved(vh.adapterPosition)
                } catch (e: Exception) {
                    Toast.makeText(parent.context, "Failed to delete item", Toast.LENGTH_SHORT).show()
                    view.alpha = 1.0f
                }
            }
            //controller.deleteItem(vh.adapterPosition)
            true
        }
        return vh
    }

    override fun getItemCount(): Int {
        return controller.getItemsCount()//.size
    }

    override fun onBindViewHolder(holder: GLViewHolder, position: Int) {
        val item = controller.getItem(position)
        holder.itemView.item_name.text = item.title
        holder.itemView.nutrition_facts.text=item.nutrition
    }

}class GLViewHolder(view: View) : RecyclerView.ViewHolder(view)
