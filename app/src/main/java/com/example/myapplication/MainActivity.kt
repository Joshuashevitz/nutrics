package com.example.myapplication


import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import com.example.myapplication.Database.GLRepoDatabase
import com.example.myapplication.Interfaces.IIntroInterface
import com.example.myapplication.Interfaces.IListOps
import com.example.myapplication.databinding.ExampleListBinding
import com.example.myapplication.models.RecipeModel
import com.example.myapplication.models.ListModelOps
import com.example.myapplication.models.RecipeViewModel
import com.example.myapplication.network.FoodApi
import kotlinx.android.synthetic.main.grocery_list_fragment.*
import kotlinx.coroutines.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class MainActivity : AppCompatActivity(),IIntroInterface {
    private lateinit var listItemOperations: IListOps
    private val scope = CoroutineScope(Dispatchers.Main)

    private val recipeViewModel by viewModel<RecipeViewModel>()
    private val binding by lazy {
        DataBindingUtil.setContentView<ExampleListBinding>(this, R.layout.activity_main).run {
            lifecycleOwner = this@MainActivity
            viewModel = recipeViewModel
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listItemOperations = ListModelOps(GLRepoDatabase(applicationContext))

        recipeViewModel.data.observe(this, androidx.lifecycle.Observer {
            Log.e("MainActivity", it.toString())
        }
        )


//        val foodApi = FoodApi(this)
//        scope.launch(Dispatchers.Default) {
//            val foodList = foodApi.fetchFood()
//            val exists = listItemOperations.getItems() //moved *to* this line
//            foodList.forEach { foodItem ->
//                //moved *from* this line
//                if (exists.firstOrNull { f -> f.id == foodItem.id } == null) {
//                    listItemOperations.addFoodItem(foodItem)
//                }
//            }
//            withContext(Dispatchers.Main) {
//              recycler_view.adapter?.notifyDataSetChanged()
//            }
//        }


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.nav_signin -> findNavController(R.id.nav_host_fragment).navigate(R.id.action_introFragment_to_signInFragment)
            R.id.nav_signup -> findNavController(R.id.nav_host_fragment).navigate(R.id.action_introFragment_to_sign_upfragment)
            R.id.nav_guest -> findNavController(R.id.nav_host_fragment).navigate(R.id.action_introFragment_to_groceryListFragment)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun launchSignIn() {
        findNavController(R.id.nav_host_fragment).navigate(R.id.action_introFragment_to_signInFragment)
    }

    override fun launchSignUp() {
        findNavController(R.id.nav_host_fragment).navigate(R.id.action_introFragment_to_sign_upfragment)
    }

    override fun launchCreateTempList() {
        findNavController(R.id.nav_host_fragment).navigate(R.id.action_introFragment_to_groceryListFragment)
    }

    override fun launchAccount() {
        findNavController(R.id.nav_host_fragment).navigate(R.id.action_signInFragment_to_accountFragment)
    }

    override fun launchNewList() {
        findNavController(R.id.nav_host_fragment).navigate(R.id.action_accountFragment_to_groceryListFragment)
    }

    override fun logOut() {
        findNavController(R.id.nav_host_fragment).navigate(R.id.action_accountFragment_to_introFragment)
    }

    override fun deleteItem(position: Int) {
        //try {
            //withContext(Dispatchers.IO) {
                listItemOperations.deleteItem(position)
           // }
        //} catch (e: Exception) {
          //  Toast.makeText(this@MainActivity, "Failed to delete item", Toast.LENGTH_SHORT).show()
            //throw e
       // }
        //listItemOperations.deleteItem(position)
    }

    override suspend fun getItems(): List<RecipeModel> {
        return listItemOperations.getItems()
    }

    override fun getItem(position: Int): RecipeModel {
        return listItemOperations.getItem(position)
    }

    override fun getItemsCount(): Int {
        return listItemOperations.getItemsCount()
    }

    override fun runAsync(blk: suspend () -> Unit) {
        scope.launch { blk() }
    }

    override fun onStop() {
        super.onStop()
        scope.cancel()
    }
}


