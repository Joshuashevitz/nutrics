package com.example.myapplication.Database

import androidx.room.*
import com.example.myapplication.models.RecipeModel
import java.util.*

@Dao
interface ListDao{
    @Insert
    fun addListItems(recipeItem: RecipeModel)
    @Delete
    fun deleteItem(recipeItem: RecipeModel)
    @Query("select id, title, nutrition from listItems")
    fun getGroceryList():List<RecipeModel>

}

//class UUIDconverter{
//    @TypeConverter
//    fun fromString(uuid: String): UUID {
//        return UUID.fromString(uuid)
//    }
//    @TypeConverter
//    fun toString(uuid: UUID): String {
//        return uuid.toString()
//    }
//}
@Database(entities = [RecipeModel::class],version = 1,exportSchema = false)
//@TypeConverters(UUIDconverter::class)
abstract class GroceryListDatabase:RoomDatabase(){
    abstract fun listDao(): ListDao
}