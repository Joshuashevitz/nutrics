package com.example.myapplication.Database

import android.content.Context
import androidx.room.Room
import com.example.myapplication.Interfaces.ListRepo
import com.example.myapplication.models.RecipeModel

class GLRepoDatabase(ctx: Context):ListRepo {
    private val GRecipe: MutableList<RecipeModel> = mutableListOf()
    private val database: GroceryListDatabase = Room.databaseBuilder(
        ctx,
        GroceryListDatabase::class.java,
        "grocerylist.db"
    ).allowMainThreadQueries().build()

    init {
        val glist:List<RecipeModel> = database.listDao().getGroceryList()
        GRecipe.addAll(glist)
    }
    override suspend fun getItems(): List<RecipeModel> {
        return GRecipe
    }

    override fun getItem(position: Int): RecipeModel {
        return GRecipe[position]
    }

    override fun getCount(): Int {
        return GRecipe.size
    }

    override fun deleteItem(position: Int) {
        val deleteGI = GRecipe[position]
        database.listDao().deleteItem(deleteGI)
    }

    override fun addFoodItem(food: RecipeModel) {
        database.listDao().addListItems(food)
    }
}