package com.example.myapplication.Database

import androidx.room.*
import com.example.myapplication.models.Account
import java.util.*

@Dao
interface AccountDao {
    @Insert
    fun addAccount(account: Account)

    @Update
    fun updateAccount(account: Account)

    @Delete
    fun deleteAccount(account: Account)

    @Query("select * from account")
    fun getAllAccounts(): List<Account>
}

class UUIDConvert {
    @TypeConverter
    fun fromString(uuid: String): UUID {
        return UUID.fromString(uuid)
    }

    @TypeConverter
    fun toString(uuid: UUID): String {
        return uuid.toString()
    }
}

@Database(entities = [Account::class], version = 1, exportSchema = false)
@TypeConverters(UUIDConvert::class)
abstract class AccountDatabase : RoomDatabase() {
    abstract fun accountDao(): AccountDao
}