package com.example.myapplication.Database

import android.content.Context
import androidx.room.Room
import com.example.myapplication.Interfaces.IAccountRepository
import com.example.myapplication.models.Account

class AccountDatabaseRepository (ctx: Context) : IAccountRepository {

    private val accountList: MutableList<Account> = mutableListOf()
    private val db: AccountDatabase = Room.databaseBuilder(
        ctx,
        AccountDatabase::class.java,
        "accounts.db"
    ).allowMainThreadQueries().build()

    init {
        clearAndFillAccountList()
    }

    override fun getCount(): Int {
        return accountList.size
    }

    override fun getAccount(idx: Int): Account {
        return accountList[idx]
    }

    override fun getAccounts(): List<Account> {
        return accountList
    }

    override fun deleteAccount(idx: Int) {
        val account = accountList[idx]
        db.accountDao().deleteAccount(account)
        clearAndFillAccountList()
    }

    override fun replaceAccount(idx: Int, account: Account) {
        db.accountDao().updateAccount(account)
        clearAndFillAccountList()
    }

    override fun addAccount(account: Account) {
        db.accountDao().addAccount(account)
        clearAndFillAccountList()
    }

    private fun clearAndFillAccountList() {
        accountList.clear()
        accountList.addAll(db.accountDao().getAllAccounts())
    }
}
