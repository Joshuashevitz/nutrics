package com.example.myapplication.Fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.myapplication.Interfaces.IIntroInterface
import com.example.myapplication.R
import com.example.myapplication.models.Account
import either.Either
import kotlinx.android.synthetic.main.sign_up_fragment.*
import java.util.*

class SignUpFragment:Fragment() {
    private lateinit var signupController: IIntroInterface
    //private var currentAccountId: UUID? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        when(context) {
            is IIntroInterface -> signupController = context
            else -> throw Exception("context did not implement signupController")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.sign_up_fragment, container, false)
    }

    /* TODO - on click listener that adds a new account from view and goes to Account page
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        signup_btn.setOnClickListener {
            when(val account = getAccountFromView()) {
                is Either.Right -> {
                    signupController.addNewAccount(account.value)
                    //something(?)
                }
            }
        }
    }
     */

    /* TODO - Make a new account from the sign up page
    private fun getAccountFromView(): Either<String, Account> {
        val username = new_username.editableText.toString()
        val password = new_password.editableText.toString()
        val email = "sample@email.com" //TODO - Add email field on sign_up_fragment.xml to pull from
        var id = if(currentAccountId == null){
            UUID.randomUUID()
        } else {
            currentAccountId!!
        }
        return Either.Right(
            Account(
                id = id,
                username = username,
                password = password,
                email = email//,
            )
        )
    }
     */

}