package com.example.myapplication.Fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.Adapters.GroceryListAdapter
import com.example.myapplication.Interfaces.IIntroInterface
import com.example.myapplication.R
import kotlinx.android.synthetic.main.grocery_list_fragment.*

class GroceryListFragment: Fragment() {
private lateinit var groceryListController : IIntroInterface

    override fun onAttach(context: Context) {
        super.onAttach(context)
        when (context) {
            is IIntroInterface -> groceryListController = context
            else -> throw Exception("context didn't implement the Controller for grocery list adapter")
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.grocery_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        add_item_btn.setOnClickListener {  }

        val adapter = GroceryListAdapter(groceryListController)

        recycler_view.adapter = adapter

        recycler_view.layoutManager = LinearLayoutManager(view.context)
    }
}