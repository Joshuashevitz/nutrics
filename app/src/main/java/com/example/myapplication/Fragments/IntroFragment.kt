package com.example.myapplication.Fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.myapplication.Interfaces.IIntroInterface
import com.example.myapplication.R
import kotlinx.android.synthetic.main.intro_fragment.*

class IntroFragment: Fragment() {
    private lateinit var introController:IIntroInterface

    override fun onAttach(context: Context) {
        super.onAttach(context)
        when(context) {
            is IIntroInterface-> introController = context
            else -> throw Exception("context didn't implement the Controller for Intro")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.intro_fragment,container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sign_in_btn.setOnClickListener { introController.launchSignIn() }
        new_list_btn_guest.setOnClickListener { introController.launchCreateTempList() }
        sign_up_btn.setOnClickListener { introController.launchSignUp() }
    }

}