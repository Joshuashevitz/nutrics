package com.example.myapplication.Fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.myapplication.Interfaces.IIntroInterface
import com.example.myapplication.R
import kotlinx.android.synthetic.main.account_fragment.*
import kotlinx.android.synthetic.main.sign_in_fragment.*
import java.lang.Exception

class AccountFragment : Fragment() {

    private lateinit var accountController: IIntroInterface

    override fun onAttach(context: Context) {
        super.onAttach(context)
        when(context) {
            is IIntroInterface -> accountController = context
            else -> throw Exception("context did not implement IIntroInterface")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.account_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        signOutBtn.setOnClickListener {
            accountController.logOut()
        }
        newListBtn.setOnClickListener {
            accountController.launchNewList()
        }
    }
    //TODO - Implement sending over name/email/both to text fields
    //TODO - Create some sort of account system (probably going to have to make an Account.kt class and set up a database)
}