package com.example.myapplication.Fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.example.myapplication.Interfaces.IIntroInterface
import com.example.myapplication.R
import kotlinx.android.synthetic.main.sign_in_fragment.*


class SignInFragment : Fragment() {
    private lateinit var signinController: IIntroInterface

    override fun onAttach(context: Context) {
        super.onAttach(context)
        when (context) {
            is IIntroInterface -> signinController = context
            else -> throw Exception("context didn't implement the Controller for Intro")
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.sign_in_fragment,container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sign_in_btn.setOnClickListener {
            //TODO - Update conditional to check if the username/password combo matches the database
            if(!usernameverify.editableText.toString().isEmpty() && !passwordverify.editableText.toString().isEmpty()) {
                signinController.launchAccount()
            }
        }
    }
}