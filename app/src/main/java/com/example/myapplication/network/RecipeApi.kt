package com.example.myapplication.network

import com.example.myapplication.models.Recipe
import retrofit2.Response
import retrofit2.http.GET

interface RecipeApi {
    @GET("recipes")
    suspend fun getUserAsync(): Response<List<Recipe>>
}