package com.example.myapplication.network


import com.example.myapplication.Interfaces.IIntroInterface
import com.example.myapplication.models.RecipeModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request


interface IFoodApi{
    suspend fun fetchFood():List<RecipeModel>

}

class FoodApi(private val controller: IIntroInterface):IFoodApi{

    override suspend fun fetchFood(): List<RecipeModel> {
        return withContext(Dispatchers.IO) {
            val client = OkHttpClient()

            val request = Request.Builder()
                    .url("https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/random?number=10")
                    .get()
                    .addHeader("x-rapidapi-key", "89ff1070c4msh7eb05b09ca0db34p1c398djsndaca010bc362")
                    .addHeader("x-rapidapi-host", "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com")
                    .build()
            val response = client.newCall(request).execute()
            if (response.isSuccessful) {
                val foodJson = response.body?.string()
                val foodListType = object : TypeToken<List<RecipeModel>>() {}.type
                val foodItem = Gson().fromJson<List<RecipeModel>>(foodJson, foodListType)
                /*return*/ foodItem
            } else {
                //throw Exception("Could not fetch food list")
                listOf()
            }
        }
    }


}