package com.example.myapplication.models

import androidx.room.*
import java.util.*

@Entity(tableName = "account")
data class Account (
    @PrimaryKey
    val id: UUID,

    @ColumnInfo(name = "email")
    var email: String,

    @ColumnInfo(name = "username")
    var username: String,

    @ColumnInfo(name = "password")
    var password: String//,

    //TODO - Need to include their saved grocery lists/items, so maybe a list of lists(?)
    /*  @ColumnInfo(name = "saved_lists")
        var savedLists: List<Grocery Lists>
     */
)
