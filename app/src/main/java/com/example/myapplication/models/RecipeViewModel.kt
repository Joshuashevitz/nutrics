package com.example.myapplication.models

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.network.RecipeApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RecipeViewModel(private val recipeApi: RecipeApi):ViewModel() {
    private val _loading = MutableLiveData<LoadingState>()
    val loading: LiveData<LoadingState>
    get() = _loading

    private val _data = MutableLiveData<List<Recipe>>()
    val data: LiveData<List<Recipe>>
    get() = _data

    init {
        fetchData()
    }

    private fun fetchData() {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _loading.postValue(LoadingState.LOADING)
                val response = recipeApi.getUserAsync()
                if (response.isSuccessful) {
                    _data.postValue(response.body())
                } else {
                    _loading.postValue(LoadingState.error(response.message()))
                }
            }catch(e:Exception) {
                _loading.postValue(LoadingState.error(e.message))
            }
        }
    }
}