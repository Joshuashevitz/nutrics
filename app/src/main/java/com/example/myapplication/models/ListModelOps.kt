package com.example.myapplication.models

import com.example.myapplication.Interfaces.IListOps
import com.example.myapplication.Interfaces.ListRepo

class ListModelOps(private val repoList : ListRepo):IListOps{
    override fun deleteItem(position: Int) {
        repoList.deleteItem(position)
    }

    override suspend fun getItems():List<RecipeModel>{
        return repoList.getItems()
    }

    override fun getItem(position: Int):RecipeModel {
        return repoList.getItem(position)
    }

    override fun addFoodItem(food: RecipeModel) {
        repoList.addFoodItem(food)
    }

    override fun getItemsCount(): Int {
        return repoList.getCount()
    }
}