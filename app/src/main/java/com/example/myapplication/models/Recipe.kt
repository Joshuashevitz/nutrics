package com.example.myapplication.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Recipe (
        val name: String,
        val ingredients : String,
        val img : String
): Parcelable