package com.example.myapplication.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "listItems")
data class RecipeModel(
    @PrimaryKey
    val id: Int,
    @ColumnInfo(name="title")
    @SerializedName("title")
    val title:String,
    @ColumnInfo(name="nutrition")

    val nutrition:String
   // @ColumnInfo(name="favorite")
    //val isFavorite: Boolean
)
