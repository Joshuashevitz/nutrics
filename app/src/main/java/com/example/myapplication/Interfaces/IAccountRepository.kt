package com.example.myapplication.Interfaces

import com.example.myapplication.models.Account

interface IAccountRepository {
    fun getCount(): Int
    fun getAccount(idx: Int): Account
    fun getAccounts(): List<Account>
    fun deleteAccount(idx: Int)
    fun replaceAccount(idx: Int, account: Account)
    fun addAccount(account: Account)
}