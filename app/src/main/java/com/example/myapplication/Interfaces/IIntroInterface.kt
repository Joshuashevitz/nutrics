package com.example.myapplication.Interfaces

import com.example.myapplication.models.RecipeModel

interface IIntroInterface {
    fun launchSignIn()
    fun launchSignUp()
    fun launchCreateTempList()
    fun deleteItem(position: Int)
    suspend fun getItems(): List<RecipeModel>
    fun getItem(position: Int): RecipeModel
    fun getItemsCount(): Int
    //async
    fun runAsync(blk: suspend () -> Unit)
    //Methods created For the account page:
    fun launchAccount()
    fun launchNewList()
    fun logOut()
}