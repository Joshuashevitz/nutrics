package com.example.myapplication.Interfaces

import com.example.myapplication.models.RecipeModel

interface ListRepo {
    suspend fun getItems(): List<RecipeModel>
    fun getItem(position:Int): RecipeModel
    fun getCount(): Int
    fun deleteItem(position:Int)
    fun addFoodItem(food:RecipeModel)
}