package com.example.myapplication.Interfaces

import com.example.myapplication.models.RecipeModel

interface IListOps {
    fun deleteItem(position:Int)
    suspend fun getItems():List<RecipeModel>
    fun getItem(position: Int):RecipeModel
    fun addFoodItem(food: RecipeModel)
    fun getItemsCount(): Int
}